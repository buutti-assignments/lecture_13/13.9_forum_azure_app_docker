
import pg from 'pg'

const { PG_HOST, PG_PORT, PG_USERNAME, PG_PASSWORD, PG_DATABASE } = process.env

export const pool = new pg.Pool({
    host: PG_HOST,
    port: Number(PG_PORT),
    user: PG_USERNAME,
    password: PG_PASSWORD,
    database: PG_DATABASE,
    // ssl: NODE_ENV === 'production'
})

export const executeQuery = async (query: string, parameters?: Array<any>) => {
    const client = await pool.connect()
    try {
        const result = await client.query(query, parameters)
        return result
    } catch (error: any) {
        console.error(error.stack)
        error.name = 'dbError'
        throw error
    } finally {
        client.release()
    }
}

export const createProductsTable = async () => {
    const query = `
        
    CREATE TABLE IF NOT EXISTS "users" (
      user_id SERIAL PRIMARY KEY ,
      username VARCHAR(50) NOT NULL,
      full_name VARCHAR(100) NOT NULL,
      email VARCHAR(100) NOT NULL,
      password_hash VARCHAR(255));
  
      CREATE TABLE IF NOT EXISTS "posts" (
      post_id SERIAL PRIMARY KEY ,
      user_id INT NOT NULL,
      title VARCHAR(100) NOT NULL,
      content TEXT NOT NULL,
      post_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP);
   
      CREATE TABLE IF NOT EXISTS "comments" (
      comment_id SERIAL PRIMARY KEY ,
      user_id INT NOT NULL,
      post_id INT NOT NULL,
      content TEXT NOT NULL,
      comment_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP)`
    await executeQuery(query)
    console.log('Products table initialized')
}