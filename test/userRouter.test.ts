import server from '../src/server'
import request from 'supertest'
import { jest } from '@jest/globals'
import { pool } from '../src/db'

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null,
        }
    })
}



//Users tests 

describe('Testing  /users', () => {
    const mockResponse = {
      rows: [
        {
          user_id: 1,
          username: "catalina_riveros",
        },
        { user_id: 2, username: "catalina_riveros" },
      ],
    };
    beforeAll(() => {
        initializeMockPool(mockResponse)
    })
    afterAll(() => {
        jest.clearAllMocks()
    })
    it('get/users returns users id and usernames in new array of objects', async () => {
        
        const response = await request(server).get('/users')
        expect(response.body).toStrictEqual([{"id": 1, "username": "catalina_riveros"}, {"id": 2, "username": "catalina_riveros"}])
    })
   
    it('get/users/:id returns user by id number', async () => {
        
      const response = await request(server).get('/users/1')
      expect(response.body).toStrictEqual(mockResponse.rows[0])
  })

  it('post/users/ posts user and returns body', async () => {
        
    const response = await request(server).post('/users')
    .send({username: "cat", fullname:"catalina",email: "cat@mail.com" })
    expect(response.body).toStrictEqual({username: "cat", fullname:"catalina",email: "cat@mail.com" })
})

it('put/users/:id modifies user info by id and returns body', async () => {
        
    const response = await request(server).put('/users/1')
    .send({username: "catpoop"})
    expect(response.body).toStrictEqual({username: "catpoop"})
})


it('delete/users/:id deletes user info by id ', async () => {
        
    const response = await request(server).delete('/users/1')
    expect(response.body).toStrictEqual({id: 1, message: "deleted"})
})

})









